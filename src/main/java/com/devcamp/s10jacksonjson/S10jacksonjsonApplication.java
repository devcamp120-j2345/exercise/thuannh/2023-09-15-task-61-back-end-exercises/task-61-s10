package com.devcamp.s10jacksonjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S10jacksonjsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(S10jacksonjsonApplication.class, args);
	}

}
